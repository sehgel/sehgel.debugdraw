extends DebugCanvasItem
class_name DebugLine2D

var start : Vector2
var end : Vector2
var width : float
var color : Color

func _init(_start : Vector2, _end : Vector2, _width : float, _color : Color, duration : float = 0.0).(duration):
	start = _start
	end = _end
	width = _width
	color = _color
	update()

func _draw():
	draw_line(start,end,color,width)
