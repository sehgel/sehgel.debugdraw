extends Node

var debug_geometry = []
var debug_canvas_items = []

onready var line_material = preload("Materials/line_material.tres")
onready var shaded_material = preload("Materials/shaded_material.tres")

onready var canvas_material = preload("Materials/canvas_material.tres")

func clear_everything():
	for child in get_children():
		child.queue_free()
	for geom in debug_geometry:
		geom.queue_free()
		
	debug_geometry.clear()
	
#2D--------->
func draw_circle_2d(point : Vector2, radius : float, color : Color, duration : float = 0.0):
	var new_debug_circle_2d = DebugCircle2D.new(point,radius,color,duration)
	new_debug_circle_2d.material = canvas_material
	add_to_2d_buffer(new_debug_circle_2d)
	
func draw_line_2d(start : Vector2, end : Vector2, color : Color, width : float = 1.0, duration : float = 0.0):
	var new_debug_line_2d = DebugLine2D.new(start,end, width,color,duration)
	new_debug_line_2d.material = canvas_material
	add_to_2d_buffer(new_debug_line_2d)
#############
		
#3D--------->
#DRAW_QUADRATIC_PATH_3D
func draw_quadratic_bezier_path_3d(points : PoolVector3Array, segments : int, color : Color, duration : float = 0.0):
	segments = max(segments,2)
	for i in range(0,points.size()-2,2):
		var last_point = points[i]
		for x in range(segments):
			var delta = x / float(segments-1)
			var point = quadratic_bezier(points[i],points[loop_index(i+1,points.size())],points[loop_index(i+2,points.size())],delta)
			draw_line_3d(last_point,point,color,duration)
			last_point = point
#DRAW_CUBIC_PATH_3D
func draw_cubic_bezier_path_3d(points : PoolVector3Array, segments : int, color : Color, duration : float = 0.0):
	segments = max(segments,2)
	for i in range(0,points.size()-3,3):
		var last_point = points[i]
		for x in range(segments):
			var delta = x / float(segments-1)
			var point = cubic_bezier(points[i],points[loop_index(i+1,points.size())],points[loop_index(i+2,points.size())],points[loop_index(i+3,points.size())],delta)
			draw_line_3d(last_point,point,color,duration)
			last_point = point
#DRAW_QUADRATIC_BEZIER_3D
func draw_quadratic_bezier_3d(p0 : Vector3, p1 : Vector3, p2 : Vector3, segments : int, color : Color, duration : float = 0.0):
	segments = max(segments,1)
	var last_point = p0
	for x in range(segments):
		var delta = x / float(segments-1)
		var point = quadratic_bezier(p0,p1,p2,delta)
		draw_line_3d(last_point,point,color,duration)
		last_point = point
#DRAW_CUBIC_BEZIER_3D
func draw_cubic_bezier_3d(p0 : Vector3, p1 : Vector3, p2 : Vector3, p3 : Vector3, segments : int, color : Color, duration : float = 0.0):
	segments = max(segments,1)
	var last_point = p0
	for x in range(segments):
		var delta = x / float(segments-1)
		var point = cubic_bezier(p0,p1,p2,p3,delta)
		draw_line_3d(last_point,point,color,duration)
		last_point = point
#DRAW_LINE_3D
func draw_line_3d(start : Vector3, end : Vector3, color : Color, duration : float = 0.0):
	var new_debug_line = DebugLine.new(start,end,color,duration)
	new_debug_line.material_override = line_material
	add_to_buffer(new_debug_line)
#DRAW_RAY_3D
func draw_ray_3d(start : Vector3, direction : Vector3, color : Color, duration : float = 0.0):
	var new_debug_line = DebugLine.new(start,start + direction,color,duration)
	new_debug_line.material_override = line_material
	add_to_buffer(new_debug_line)
#DRAW_SPHERE
func draw_solid_sphere(point : Vector3, radius : float, color : Color, subdivisions : int = 16, duration : float = 0.0):
	var new_debug_sphere = DebugSphere.new(point,radius,color,subdivisions,duration)
	new_debug_sphere.material_override = shaded_material
	add_to_buffer(new_debug_sphere)
#DRAW_SOLID_CUBOID
func draw_solid_cuboid(point : Vector3, size : Vector3, color : Color, duration : float = 0.0):
	var new_debug_cuboid = DebugCuboid.new(point,size,color,duration)
	new_debug_cuboid.material_override = shaded_material
	add_to_buffer(new_debug_cuboid)
#DRAW_SOLID_CUBE
func draw_solid_cube(point : Vector3, size : float, color : Color, duration : float = 0.0):
	var new_debug_cube = DebugCuboid.new(point,Vector3.ONE*size,color,duration)
	new_debug_cube.material_override = shaded_material
	add_to_buffer(new_debug_cube)
#DRAW_WIRED_CUBOID
func draw_wired_cuboid(point : Vector3, size : Vector3, color : Color, duration : float = 0.0):
	var new_debug_wired_cuboid = DebugWiredCuboid.new(point,size,color,duration)
	new_debug_wired_cuboid.material_override = line_material
	add_to_buffer(new_debug_wired_cuboid)
#DRAW_WIRED_CUBE
func draw_wired_cube(point : Vector3, size : float, color : Color, duration : float = 0.0):
	var new_debug_wired_cuboid = DebugWiredCuboid.new(point,Vector3.ONE * size,color,duration)
	new_debug_wired_cuboid.material_override = line_material
	add_to_buffer(new_debug_wired_cuboid)
#DRAW_PIN_3D
func draw_pin_3d(point : Vector3, direction : Vector3, radius : float, height : float, color : Color, duration : float = 0.0):
	var new_debug_pint = DebugPin.new(point,direction,radius,height,color,duration)
	new_debug_pint.material_override = shaded_material
	add_to_buffer(new_debug_pint)
#<-----------3D

func add_to_buffer(debug_geometry_instance : DebugGeometry):
	#Don't add if it's persistant
	add_child(debug_geometry_instance)
	if debug_geometry_instance.time_left <= -1:
		return
		
	debug_geometry.append(debug_geometry_instance)
	
func add_to_2d_buffer(debug_canvas_item : DebugCanvasItem):
	#Don't add if it's persistant
	add_child(debug_canvas_item)
	if debug_canvas_item.time_left <= -1:
		return
		
	debug_canvas_items.append(debug_canvas_item)
	
func _process(delta):
	process_debug_geometry(delta)
	
func process_debug_geometry(delta):
	#3D
	for i in range(debug_geometry.size()-1,-1,-1):
		debug_geometry[i].time_left -= delta
		if debug_geometry[i].time_left <= 0.0:
			debug_geometry[i].queue_free()
			debug_geometry.remove(i)
	#2D
	for i in range(debug_canvas_items.size()-1,-1,-1):
		debug_canvas_items[i].time_left -= delta
		if debug_canvas_items[i].time_left <= 0.0:
			debug_canvas_items[i].queue_free()
			debug_canvas_items.remove(i)

#Math helpers
func quadratic_bezier(p1, p2, p3, t):
	var q0 = p1.linear_interpolate(p2,t)
	var q1 = p2.linear_interpolate(p3,t)
	return q0.linear_interpolate(q1,t)
	
func cubic_bezier(p1,p2,p3,p4,t):
	
	var r0 = quadratic_bezier(p1,p2,p3,t)
	var r1 = quadratic_bezier(p2,p3,p4,t)
	
	return r0.linear_interpolate(r1,t)
	
func loop_index(index : int, size : int):
	return (index + size) % size
