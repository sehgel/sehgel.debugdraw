extends DebugGeometry
class_name DebugSphere
func _init(point : Vector3, radius : float, _color : Color, subdivisions : int = 16, duration : float = 0.0).(duration):
	#DRAW
	for i in range(subdivisions+1):
		begin(Mesh.PRIMITIVE_TRIANGLE_STRIP)
		set_color(_color)
		var latitude = range_lerp(i,0,subdivisions,-PI/2.0,PI/2.0)
		var next_latitude = range_lerp(i+1,0,subdivisions,-PI/2.0,PI/2.0)
		for j in range(subdivisions+1):
			var longitude = range_lerp(j,subdivisions,0,-PI,PI) if i <= subdivisions/2 else range_lerp(j,0,subdivisions,-PI,PI)
			var next_longitude = range_lerp(j+1,0,subdivisions,-PI,PI)
			
			var signed = -1 if j > subdivisions/2 else 1
			var x = radius * sin(longitude) * cos(latitude)
			var y = radius * sin(longitude) * sin(latitude)
			var z = radius * cos(longitude)

			var next_x = radius * sin(longitude) * cos(next_latitude)
			var next_y = radius * sin(longitude) * sin(next_latitude)
			var next_z = radius * cos(longitude)
			
			set_normal((Vector3(x,y,z) - point).normalized())
			add_vertex(Vector3(x,y,z) + point)
			add_vertex(Vector3(next_x,next_y,next_z) + point)
		end()
