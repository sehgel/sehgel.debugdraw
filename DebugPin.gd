extends DebugGeometry
class_name DebugPin

func _init(point : Vector3, direction : Vector3, radius : float, height : float, color : Color, duration : float = 0.0).(duration):
	#DRAW
	var point_count = clamp(radius,8,32)
	var normal = Vector3(direction.z,direction.x,direction.y)
	var binormal = direction.cross(normal)
#	print("Direction: %s, Normal: %s, Binormal: %s" % [direction,normal,binormal])
	begin(Mesh.PRIMITIVE_TRIANGLES)
	for i in range(0,point_count,1):
		set_color(color)
		var t = i / float(point_count)
		var t2 = (i+1) / float(point_count)
		var angle = lerp(0,PI*2,t)
		var angle2 = lerp(0,PI*2,t2)
		
		var p0 = point + binormal.rotated(direction,angle)*radius - direction*height
		var p1 = point + binormal.rotated(direction,angle2)*radius - direction*height
		
		set_normal((p0 - point).cross(p1 - point))
		add_vertex(p1)
		add_vertex(p0)
		add_vertex(point)
	end()
	begin(Mesh.PRIMITIVE_TRIANGLE_FAN)
	for i in range(point_count):
		set_color(color)
		var t = i / float(point_count)
		var angle = lerp(0,PI*2,t)
		
		var p = binormal.rotated(direction,angle)*radius
		
		set_normal(-direction)
		add_vertex(point + p - direction*height)
#		print("X: %s, Y: %s" % [x,y])
	end()

