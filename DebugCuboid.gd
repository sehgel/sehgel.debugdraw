extends DebugGeometry
class_name DebugCuboid

func _init(position : Vector3, size : Vector3, color : Color, duration : float).(duration):
	var half_size = size / 2.0
	#DRAW
	begin(Mesh.PRIMITIVE_TRIANGLES)
	set_color(color)
	
	var top_left_back = position + Vector3.LEFT * half_size.x + Vector3.BACK * half_size.z + Vector3.UP* half_size.y
	var top_right_back = position + Vector3.RIGHT * half_size.x + Vector3.BACK * half_size.z + Vector3.UP* half_size.y
	var bottom_left_back = position + Vector3.LEFT * half_size.x + Vector3.BACK * half_size.z + Vector3.DOWN* half_size.y
	var bottom_right_back = position + Vector3.RIGHT * half_size.x + Vector3.BACK * half_size.z + Vector3.DOWN* half_size.y
	
	var top_left_front = position + Vector3.LEFT * half_size.x + Vector3.FORWARD * half_size.z + Vector3.UP* half_size.y
	var top_right_front = position + Vector3.RIGHT * half_size.x + Vector3.FORWARD * half_size.z + Vector3.UP* half_size.y
	var bottom_left_front = position + Vector3.LEFT * half_size.x + Vector3.FORWARD * half_size.z + Vector3.DOWN* half_size.y
	var bottom_right_front = position + Vector3.RIGHT * half_size.x + Vector3.FORWARD * half_size.z + Vector3.DOWN* half_size.y
	
	#Back face
	set_normal(Vector3.BACK)
	#0
	add_vertex(top_left_back)
	add_vertex(top_right_back)
	add_vertex(bottom_left_back)
	#1
	add_vertex(top_right_back)
	add_vertex(bottom_right_back)
	add_vertex(bottom_left_back)

	#Right face
	set_normal(Vector3.RIGHT)
	#0
	add_vertex(top_right_back)
	add_vertex(top_right_front)
	add_vertex(bottom_right_back)
	#1
	add_vertex(bottom_right_back)
	add_vertex(top_right_front)
	add_vertex(bottom_right_front)
	#Left face
	set_normal(Vector3.LEFT)
	#1
	add_vertex(bottom_left_front)
	add_vertex(top_left_front)
	add_vertex(top_left_back)
	#2
	add_vertex(bottom_left_front)
	add_vertex(top_left_back)
	add_vertex(bottom_left_back)
	#Front face
	set_normal(Vector3.FORWARD)
	#1
	add_vertex(bottom_right_front)
	add_vertex(top_right_front)
	add_vertex(top_left_front)
	#2
	add_vertex(bottom_right_front)
	add_vertex(top_left_front)
	add_vertex(bottom_left_front)
	#Bottom face
	set_normal(Vector3.DOWN)
	#1
	add_vertex(bottom_right_back)
	add_vertex(bottom_right_front)
	add_vertex(bottom_left_back)
	#2
	add_vertex(bottom_right_front)
	add_vertex(bottom_left_front)
	add_vertex(bottom_left_back)
	#Top face
	set_normal(Vector3.UP)
	#1
	add_vertex(top_left_back)
	add_vertex(top_left_front)
	add_vertex(top_right_front)
	#2
	add_vertex(top_left_back)
	add_vertex(top_right_front)
	add_vertex(top_right_back)
	end()
