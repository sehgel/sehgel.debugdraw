extends DebugGeometry
class_name DebugWiredCuboid

func _init(position : Vector3, size : Vector3, color : Color, duration : float).(duration):
	var half_size = size / 2.0
	#DRAW
	begin(Mesh.PRIMITIVE_LINES)
	set_color(color)
	
	var top_left_back = position + Vector3.LEFT * half_size.x + Vector3.BACK * half_size.z + Vector3.UP* half_size.y
	var top_right_back = position + Vector3.RIGHT * half_size.x + Vector3.BACK * half_size.z + Vector3.UP* half_size.y
	var bottom_left_back = position + Vector3.LEFT * half_size.x + Vector3.BACK * half_size.z + Vector3.DOWN* half_size.y
	var bottom_right_back = position + Vector3.RIGHT * half_size.x + Vector3.BACK * half_size.z + Vector3.DOWN* half_size.y
	
	var top_left_front = position + Vector3.LEFT * half_size.x + Vector3.FORWARD * half_size.z + Vector3.UP* half_size.y
	var top_right_front = position + Vector3.RIGHT * half_size.x + Vector3.FORWARD * half_size.z + Vector3.UP* half_size.y
	var bottom_left_front = position + Vector3.LEFT * half_size.x + Vector3.FORWARD * half_size.z + Vector3.DOWN* half_size.y
	var bottom_right_front = position + Vector3.RIGHT * half_size.x + Vector3.FORWARD * half_size.z + Vector3.DOWN* half_size.y
	
	add_vertex(top_left_back)
	add_vertex(top_right_back)
	
	add_vertex(top_right_back)
	add_vertex(top_right_front)
	
	add_vertex(top_right_front)
	add_vertex(top_left_front)
	
	add_vertex(top_left_front)
	add_vertex(top_left_back)
	
	add_vertex(bottom_left_back)
	add_vertex(bottom_right_back)
	
	add_vertex(bottom_right_back)
	add_vertex(bottom_right_front)
	
	add_vertex(bottom_right_front)
	add_vertex(bottom_left_front)
	
	add_vertex(bottom_left_front)
	add_vertex(bottom_left_back)
	
	add_vertex(bottom_left_back)
	add_vertex(top_left_back)
	
	add_vertex(bottom_right_back)
	add_vertex(top_right_back)
	
	add_vertex(bottom_left_front)
	add_vertex(top_left_front)
	
	add_vertex(bottom_right_front)
	add_vertex(top_right_front)
	
	end()
