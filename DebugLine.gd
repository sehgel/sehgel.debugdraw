extends DebugGeometry
class_name DebugLine

func _init(_start : Vector3,_end : Vector3, _color : Color, duration : float).(duration):
	#DRAW
	begin(Mesh.PRIMITIVE_LINES)
	set_color(_color)
	add_vertex(_start)
	add_vertex(_end)
	end()
