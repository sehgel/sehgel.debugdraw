extends ImmediateGeometry
class_name DebugGeometry

var time_left : float
func _init(duration : float):
	time_left = duration
	cast_shadow = false
