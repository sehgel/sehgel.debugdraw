extends DebugCanvasItem
class_name DebugCircle2D

var point : Vector2
var radius : float
var color : Color

func _init(_point : Vector2, _radius : float, _color : Color, duration : float = 0.0).(duration):
	point = _point
	radius = _radius
	color = _color
	update()

func _draw():
	draw_circle(point,radius,color)
